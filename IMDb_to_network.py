import os
import pickle

from classes import Film, Person, Edge

pickle_folder = 'pickled_data/'

en_chars = 'abcdefghijklmnopqrstuvwxyz' \
           'ABCDEFGHIJKLMNOPQRSTUVWXYZ' \
           '0123456789 .,-()!?+-\''


def is_in_english(text):
    for c in text:
        if c not in en_chars:
            return False
    return True


def construct_network():
    edges = []
    pickled_path = pickle_folder + 'whole_network'

    if not os.path.isfile(pickled_path):
        films = {}
        people = {}

        print('Reading films.')
        with open('data/title.basics.tsv') as f:
            indexes = ['tconst', 'titleType', 'primaryTitle', 'startYear', 'genres']

            for i, line in enumerate(f):
                line = line.strip().split('\t')
                if i == 0:
                    indexes = {col_name: line.index(col_name) for col_name in indexes}
                else:
                    # Must be 'movie'. Also other constraints can be added.
                    if line[indexes['titleType']] != 'movie': continue

                    # Only new movies, can be removed in the future.
                    if len(line[indexes['startYear']]) < 4 or int(line[indexes['startYear']]) < 1950: continue

                    # Only English
                    if not is_in_english(line[indexes['primaryTitle']]): continue

                    films[line[indexes['tconst']]] = Film(line[indexes['primaryTitle']],
                                                          line[indexes['startYear']],
                                                          line[indexes['genres']])

        # TODO: maybe remove this section, because a lot of films are removed
        print('Reading title.akas.tsv.')
        with open('data/title.akas.tsv') as f:
            indexes = ['titleId', 'isOriginalTitle', 'language']

            for i, line in enumerate(f):
                line = line.strip().split('\t')
                if i == 0:
                    indexes = {col_name: line.index(col_name) for col_name in indexes}
                else:
                    film = films.get(line[indexes['titleId']])
                    if not film: continue

                    if line[indexes['isOriginalTitle']] == '0': continue

                    if line[indexes['language']] != 'en' and line[indexes['language']] != '\\N':
                        del films[line[indexes['titleId']]]

        print('Reading ratings.')
        with open('data/title.ratings.tsv') as f:
            indexes = ['tconst', 'averageRating', 'numVotes']

            for i, line in enumerate(f):
                line = line.strip().split('\t')
                if i == 0:
                    indexes = {col_name: line.index(col_name) for col_name in indexes}
                else:
                    film = films.get(line[indexes['tconst']])
                    if not film: continue

                    film.avg_rating = float(line[indexes['averageRating']])
                    film.no_votes = int(line[indexes['numVotes']])

        print('Reading people.')
        with open('data/title.principals.tsv') as f:
            indexes = ['tconst', 'nconst', 'category', 'job']

            for i, line in enumerate(f):
                line = line.strip().split('\t')
                if i == 0:
                    indexes = {col_name: line.index(col_name) for col_name in indexes}
                else:

                    film = films.get(line[indexes['tconst']])
                    if not film: continue

                    # Getting person.
                    nconst = line[indexes['nconst']]
                    if nconst in people:
                        person = people[nconst]
                    else:
                        person = Person()
                        people[nconst] = person

                    # Adding edge.
                    edge = Edge(film,
                                person,
                                category=line[indexes['category']],
                                job=line[indexes['job']])
                    edges.append(edge)

        print('Reading name.basics.tsv.')
        with open('data/name.basics.tsv') as f:
            indexes = ['nconst', 'primaryName', 'knownForTitles']

            for i, line in enumerate(f):
                line = line.strip().split('\t')
                if i == 0:
                    indexes = {col_name: line.index(col_name) for col_name in indexes}
                else:

                    # Person must exist.
                    person = people.get(line[indexes['nconst']])
                    if not person: continue

                    person.name = line[indexes['primaryName']]

                    # known_for
                    for tconst in line[indexes['knownForTitles']].split(','):
                        if tconst in films:
                            person.known_for.append(films[tconst])

        # pickling
        with open(pickled_path, 'wb') as f:
            pickle.dump(edges, f)

    else:
        print('Unpickling (1).')
        with open(pickled_path, 'rb') as f:
            edges = pickle.load(f)

    films = set()
    people = set()

    for edge in edges:
        edge.film.edges.append(edge)
        edge.person.edges.append(edge)
        films.add(edge.film)
        people.add(edge.person)

    print('Finished reading data.')
    print('  no. films: ', len(films))
    print('  no. people:', len(people))
    print('  no. edges: ', len(edges))

    return list(films), list(people), edges


if __name__ == '__main__':
    construct_network()
