class Film:
    id_counter = 0

    def __init__(self, title, year, genres):
        self.title = title
        self.year = year
        self.avg_rating = 0
        self.no_votes = 0
        self.edges = []
        self.genres = set(genres.split(','))

        self.id = Film.id_counter
        Film.id_counter += 1

    def __str__(self):
        return self.title

    def __eq__(self, other):
        if isinstance(other, Film):
            return self.id == other.id
        return False

    def __ne__(self, other):
        return not self.__eq__(other)

    def __hash__(self):
        return self.id


class Person:
    id_counter = 0

    def __init__(self):
        self.edges = []
        self.name = ''
        self.known_for = []

        self.id = Person.id_counter
        Person.id_counter += 1

    def __str__(self):
        return self.name

    def __eq__(self, other):
        if isinstance(other, Person):
            return self.id == other.id
        return False

    def __ne__(self, other):
        return not self.__eq__(other)

    def __hash__(self):
        return self.id


class Edge:
    def __init__(self, film, person, *, category='', job=''):
        self.film = film
        self.person = person
        self.category = category
        self.job = job

    def __str__(self):
        return '{name} - {role}'.format(name=self.person.name,
                                        role=self.category)
