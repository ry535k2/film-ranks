# Predicting Films' Ratings

See `doc/report.pdf` for details about this project.

We are using Python 3.6.

Install requirements with
```
pip install -r requirements.txt
```

In root directory (`film-ranks`) run with
```
python3 -m node2vec.main
```

Download thse files from [IMDb](https://www.imdb.com/interfaces/):


#### title.ratings.tsv.gz

Contains the following information for names:

* **tconst** (string) - alphanumeric unique identifier of the title
* **averageRating** - weighted average of all the individual user ratings
* **numVotes** - number of votes the title has received


#### title.principals.tsv.gz

Contains the principal cast/crew for titles

* **tconst** (string) - alphanumeric unique identifier of the title
* **nconst** (string) - alphanumeric unique identifier of the name/person
* **category** (string) - the category of job that person was in
* **job** (string) - the specific job title if applicable, else '\N'

Not used:
* **characters** (string) - the name of the character played if applicable, else '\N'
* **ordering** (integer) - a number to uniquely identify rows for a given titleId


#### title.akas.tsv.gz

* **titleId** (string) - a tconst, an alphanumeric unique identifier of the title
* **isOriginalTitle** (boolean) - 0: not original title; 1: original title
* **language** (string) - the language of the title

Not used:
* **ordering** (integer) - a number to uniquely identify rows for a given titleId
* **title** (string) - the localized title
* **region** (string) - the region for this version of the title
* **types** (array) - Enumerated set of attributes for this alternative title. One or more of the following: "alternative", "dvd", "festival", "tv", "video", "working", "original", "imdbDisplay". New values may be added in the future without warning
* **attributes** (array) - Additional terms to describe this alternative title, not enumerated



#### title.crew.tsv.gz for directors

Contains the director and writer information for all the titles in IMDb. Fields include:

* **tconst** (string) - alphanumeric unique identifier of the title
* **directors** (array of nconsts) - director(s) of the given title
* **writers** (array of nconsts) - writer(s) of the given title


#### name.basics.tsv.gz

Contains the following information for names:

* **nconst** (string) - alphanumeric unique identifier of the name/person
* **primaryName** (string) - name by which the person is most often credited
* **knownForTitles** (array of tconsts) - titles the person is known for

Not used:
* **birthYear** - in YYYY format
* **deathYear** - in YYYY format if applicable, else '\N'
* **primaryProfession** (array of strings) - the top-3 professions of the person


#### title.basics.tsv.gz

Contains the following information for titles:

* **tconst** (string) - alphanumeric unique identifier of the title
* **titleType** (string) - the type/format of the title (e.g. movie, short, tvseries, tvepisode, video, etc)
* **primaryTitle** (string) - the more popular title / the title used by the filmmakers on promotional materials at the point of release
* **isAdult** (boolean) - 0: non-adult title; 1: adult title
* **startYear** (YYYY) - represents the release year of a title. In the case of TV Series, it is the series start year

Not used:
* **originalTitle** (string) - original title, in the original language
* **endYear** (YYYY) - TV Series end year. ‘\N’ for all other title types
* **runtimeMinutes** - primary runtime of the title, in minutes
* **genres** (string array) - includes up to three genres associated with the title









