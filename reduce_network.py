import os
import pickle

from IMDb_to_network import pickle_folder, construct_network
from classes import Edge


def read(*, films_max=None, no_votes_min=0, known_for_min=0):
    pickled_path = pickle_folder + 'films_{0}_votes_{1}_known_{2}'.format(str(films_max), str(no_votes_min), str(known_for_min))

    if not os.path.isfile(pickled_path):
        films, people, _ = construct_network()

        edges = []
        for film in films:
            film.edges = []

        films = set()
        for person in people:
            person.edges = []
            if len(person.known_for) >= known_for_min:
                for film in person.known_for:
                    if film.no_votes >= no_votes_min:
                        edges.append(Edge(film, person))
                        films.add(film)
            if films_max is not None and len(films) >= films_max: break

        # pickling
        with open(pickled_path, 'wb') as f:
            pickle.dump(edges, f)

    else:
        print('Unpickling (2).')
        with open(pickled_path, 'rb') as f:
            edges = pickle.load(f)

    films = set()
    people = set()

    for edge in edges:
        edge.film.edges.append(edge)
        edge.person.edges.append(edge)
        films.add(edge.film)
        people.add(edge.person)

    print('Finished reducing network.')
    print('  no. films: ', len(films))
    print('  no. people:', len(people))
    print('  no. edges: ', len(edges))

    # TODO: Plot degree distribution, size of the biggest component, print avg. rating, variance of ratings

    return list(films), list(people), edges


if __name__ == '__main__':
    read(films_max=1000, no_votes_min=0, known_for_min=4)
