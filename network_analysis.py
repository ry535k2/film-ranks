import matplotlib.pyplot as plt
from collections import defaultdict

from IMDb_to_network import construct_network
from reduce_network import read


def degree_distribution(**kwargs):
    films, people, edges = read(**kwargs)

    plt.hist([len(film.edges) for film in films], log=True)
    plt.title('Degree distribution of films')
    plt.show()

    plt.hist([len(person.edges) for person in people], log=True)
    plt.title('Degree distribution of people')
    plt.show()


def count_known_for():
    counts = defaultdict(int)
    _, people, _ = construct_network()

    for person in people:
        counts[len(person.known_for)] += 1

    counts = sorted([(length, no) for length, no in counts.items()], key=lambda x: x[0])
    print('no. known_for,', 'no. such people')
    for length, no in counts:
        print(length, no)


def get_connected_component(film, visited):
    queue = {film}
    component = {film}
    visited.add(film)

    while len(queue) > 0:
        film = queue.pop()

        for edge in film.edges:
            person = edge.person
            for edge2 in person.edges:
                neighbor = edge2.film

                if neighbor not in visited:
                    visited.add(neighbor)
                    component.add(neighbor)
                    queue.add(neighbor)

    return component


def get_connected_components(films):
    visited = set()
    components = []

    for film in films:
        if film not in visited:
            components.append(get_connected_component(film, visited))

    components.sort(key=lambda x: -len(x))
    return [list(c) for c in components]


def analyze_components(**kwargs):
    films, _, _ = read(**kwargs)
    components = get_connected_components(films)

    counts = defaultdict(int)
    for c in components:
        counts[len(c)] += 1

    counts = sorted([(length, no) for length, no in counts.items()], key=lambda x: x[0])
    print('size of component,', 'no.')
    for length, no in counts:
        print(length, no)


if __name__ == '__main__':
    kwargs = {
        'films_max': 10000,
        'no_votes_min': 100,
        'known_for_min': 2
    }

    # degree_distribution(**kwargs)
    # count_known_for()
    analyze_components(**kwargs)
    pass



"""
v1, v2 - vozlisci

vertices = [seznam vozlisc], preveri, da id == index v seznamu

similarity = 0
for index in v1.walks:
    #kokrat_si_bil_tam = v1.walks[index]
    similarity += v2.walks[index] * v1.walks[index]

"""
