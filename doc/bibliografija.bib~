@unpublished{cs224,
	author = {Cao, Haowen and Holstein, Daniel and Lee, Casatrina},
	note = {CS224W Project report},
	school = {Stanford University},
	title = {Building a Predictor for Movie Ratings},
	year = {2014}
}

@unpublished{neural_network,
	author = {Augustine, Achal and Pathak, Manas},
	note = {Neural network for rating prediction},
	school = {University of Texas at Austin},
	title = {User rating prediction for movies}
}

@inproceedings{contextual_walk,
	author = {Bogers, Toine},
	booktitle = {Proc. of the 2nd Intl. Workshop on Context-Aware Recommender Systems},
	title = {Movie recommendation using random walks over the contextual graph},
	x-fetchedfrom = {Google Scholar},
	year = {2010}
}

@misc{node2vec,
	abstract = {Prediction tasks over nodes and edges in networks require careful effort in engineering features used by learning algorithms. Recent research in the broader field of representation learning has led to significant progress in automating prediction by learning the features themselves. However, present feature learning approaches are not expressive enough to capture the diversity of connectivity patterns observed in networks. Here we propose node2vec, an algorithmic framework for learning continuous feature representations for nodes in networks. In node2vec, we learn a mapping of nodes to a low-dimensional space of features that maximizes the likelihood of preserving network neighborhoods of nodes. We define a flexible notion of a node's network neighborhood and design a biased random walk procedure, which efficiently explores diverse neighborhoods. Our algorithm generalizes prior work which is based on rigid notions of network neighborhoods, and we argue that the added flexibility in exploring neighborhoods is the key to learning richer representations. We demonstrate the efficacy of node2vec over existing state-of-the-art techniques on multi-label classification and link prediction in several real-world networks from diverse domains. Taken together, our work represents a new way for efficiently learning state-of-the-art task-independent representations in complex networks.},
	archiveprefix = {arXiv},
	author = {Grover, Aditya and Leskovec, Jure},
	comment = {published = 2016-07-03T16:09:30Z, updated = 2016-07-03T16:09:30Z, In Proceedings of the 22nd ACM SIGKDD International Conference on Knowledge Discovery and Data Mining, 2016},
	eprint = {1607.00653v1},
	month = jul,
	primaryclass = {cs.SI},
	title = {{node2vec: Scalable Feature Learning for Networks}},
	url = {http://arxiv.org/abs/1607.00653v1; http://arxiv.org/pdf/1607.00653v1},
	x-fetchedfrom = {arXiv.org},
	year = {2016}
}

@misc{imdb,
	author = {IMDb},
	key = {https://www.imdb.com/interfaces/},
	title = {Internet movie database}
}

@misc{snap,
	abstract = {  Large networks are becoming a widely used abstraction for studying complex
systems in a broad set of disciplines, ranging from social network analysis to
molecular biology and neuroscience. Despite an increasing need to analyze and
manipulate large networks, only a limited number of tools are available for
this task.
  Here, we describe Stanford Network Analysis Platform (SNAP), a
general-purpose, high-performance system that provides easy to use, high-level
operations for analysis and manipulation of large networks. We present SNAP
functionality, describe its implementational details, and give performance
benchmarks. SNAP has been developed for single big-memory machines and it
balances the trade-off between maximum performance, compact in-memory graph
representation, and the ability to handle dynamic graphs where nodes and edges
are being added or removed over time. SNAP can process massive networks with
hundreds of millions of nodes and billions of edges. SNAP offers over 140
different graph algorithms that can efficiently manipulate large graphs,
calculate structural properties, generate regular and random graphs, and handle
attributes and meta-data on nodes and edges. Besides being able to handle large
graphs, an additional strength of SNAP is that networks and their attributes
are fully dynamic, they can be modified during the computation at low cost.
SNAP is provided as an open source library in C++ as well as a module in
Python.
  We also describe the Stanford Large Network Dataset, a set of social and
information real-world networks and datasets, which we make publicly available.
The collection is a complementary resource to our SNAP software and is widely
used for development and benchmarking of graph analytics algorithms.
},
	archiveprefix = {arXiv},
	author = {Leskovec, Jure and Sosic, Rok},
	comment = {published = 2016-06-24T03:17:12Z, updated = 2016-06-24T03:17:12Z},
	eprint = {1606.07550v1},
	month = jun,
	primaryclass = {cs.SI},
	title = {{SNAP: A General Purpose Network Analysis and Graph Mining Library}},
	url = {http://arxiv.org/abs/1606.07550v1; http://arxiv.org/pdf/1606.07550v1},
	x-fetchedfrom = {arXiv.org},
	year = {2016}
}

@misc{git_repo,
	author = {Rus, Marko and Ver{\v c}ek, Mihael and Hribernik, Toma{\v z}},
	journal = {https://gitlab.com/ry535k2/film-ranks},
	title = {Project's git repository},
	type = {https://gitlab.com/ry535k2/film-ranks}
}

@techreport{pagerank,
	abstract = {The importance of a Web page is an inherently subjective matter, which depends on the readers interests, knowledge and attitudes. But there is still much that can be said objectively about the relative importance of Web pages. This paper describes PageRank, a mathod for rating Web pages objectively and mechanically, effectively measuring the human interest and attention devoted to them. We compare PageRank to an idealized random Web surfer. We show how to efficiently compute PageRank for large numbers of pages. And, we show how to apply PageRank to search and to user navigation.},
	author = {Page, Lawrence and Brin, Sergey and Motwani, Rajeev and Winograd, Terry},
	institution = {Stanford InfoLab},
	month = {November},
	note = {Previous number = SIDL-WP-1999-0120},
	number = {1999-66},
	publisher = {Stanford InfoLab},
	title = {The PageRank Citation Ranking: Bringing Order to the Web.},
	type = {Technical Report},
	url = {http://ilpubs.stanford.edu:8090/422/},
	year = {1999}
}

@article{hits,
	acmid = {324140},
	address = {New York, NY, USA},
	author = {Kleinberg, Jon M.},
	doi = {10.1145/324133.324140},
	issn = {0004-5411},
	issue_date = {Sept. 1999},
	journal = {J. ACM},
	keywords = {World Wide Web; graph algorithms; hypertext structure; link analysis},
	month = sep,
	number = {5},
	numpages = {29},
	pages = {604--632},
	publisher = {ACM},
	title = {Authoritative Sources in a Hyperlinked Environment},
	url = {http://doi.acm.org/10.1145/324133.324140},
	volume = {46},
	year = {1999}
}

@misc{word2vec,
	abstract = {  We propose two novel model architectures for computing continuous vector
representations of words from very large data sets. The quality of these
representations is measured in a word similarity task, and the results are
compared to the previously best performing techniques based on different types
of neural networks. We observe large improvements in accuracy at much lower
computational cost, i.e. it takes less than a day to learn high quality word
vectors from a 1.6 billion words data set. Furthermore, we show that these
vectors provide state-of-the-art performance on our test set for measuring
syntactic and semantic word similarities.
},
	archiveprefix = {arXiv},
	author = {Mikolov, Tomas and Chen, Kai and Corrado, Greg and Dean, Jeffrey},
	comment = {published = 2013-01-16T18:24:43Z, updated = 2013-09-07T00:30:40Z},
	eprint = {1301.3781v3},
	month = sep,
	primaryclass = {cs.CL},
	title = {{Efficient Estimation of Word Representations in Vector Space}},
	url = {http://arxiv.org/abs/1301.3781v3; http://arxiv.org/pdf/1301.3781v3},
	x-fetchedfrom = {arXiv.org},
	year = {2013}
}

@article{gensim,
	author = {{\v R}eh{\u u}{\v r}ek, Radim and Sojka, Petr},
	title = {Gensim---Statistical Semantics in Python},
	year = {2011}
}

@article{scikit-learn,
	author = {Pedregosa, F. and Varoquaux, G. and Gramfort, A. and Michel, V. and Thirion, B. and Grisel, O. and Blondel, M. and Prettenhofer, P. and Weiss, R. and Dubourg, V. and Vanderplas, J. and Passos, A. and Cournapeau, D. and Brucher, M. and Perrot, M. and Duchesnay, E.},
	journal = {Journal of Machine Learning Research},
	pages = {2825--2830},
	title = {Scikit-learn: Machine Learning in {P}ython},
	volume = {12},
	year = {2011}
}

@article{randomforest,
	author = {Breiman, Leo},
	journal = {Machine learning},
	number = {1},
	pages = {5--32},
	publisher = {Springer},
	title = {Random forests},
	volume = {45},
	year = {2001}
}

