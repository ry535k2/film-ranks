\documentclass[a4paper,10pt]{article}
\usepackage[utf8]{inputenc}
% za naslov univerze
\usepackage[affil-it]{authblk}
% za seznam
\usepackage{scrextend}
\addtokomafont{labelinglabel}{\sffamily}
% za slikce
\usepackage{graphicx}
\usepackage[section]{placeins}
\usepackage{tabularx}
    \newcolumntype{L}{>{\raggedright\arraybackslash}X}

%opening
\title{Prediction of Movies’ Ratings}
\author{Marko Rus, Mihael Verček, Tomaž Hribernik \\
        \{mr5613, mv6463, th6727\}@student.uni-lj.si}
        
\affil{Faculty of Computer and Information Science, 
    University of Ljubljana, Ljubljana, Slovenia }

\begin{document}

\maketitle

\begin{abstract}
In this paper we study the different approaches of predicting ratings of movies, and build two predictors, one by using {\it node2vec} \cite{node2vec}, and one by simulating random walks on the network of films. We show that there exists a strong connection between movie’s rating and its cast, that {\it node2vec} may not be the most suitable approach to solve this problem, and that our model with random walks predicts better than neighbors’ average predictor. All the code used is available at Gitlab \cite{git_repo}.

\end{abstract}

\section{Introduction}
Prediction of someone's desires has undergone a lot of research in the last few years. A lot of work has been put into research of movie ratings and our goal is to build from previous findings. Film is art and its judgement is therefore very subjective. The wisdom of the crowd gives best approximation of quality of movies. Our goal is to identify the aspects of movies that primarily affect judgement of viewers. Our scope is to build a model for predicting ratings of movies based on the cast and other people that are connected to the movie. With extraction of features from the network that represent real-life dynamics we are better equipped to pass judgement whether movie is great or not. 

Our work is built upon previous research \cite{neural_network, contextual_walk, cs224} that was conducted on this domain and we are trying to expand their ideas where we can. We are implementing our own versions of {\it node2vec} \cite{node2vec}, random walks and some other simple local oriented algorithms, all of which we are testing against a real IMDb \cite{imdb} dataset. Our baseline for success evaluation are randomly reassigned movie ratings. In conclusion we summarize our findings and obstacles that we encountered during research.

\section{Related Work}
There is substantial amount of research being done in this area that we can build from. The general direction is applying different techniques from machine learning \cite{neural_network} and network analysis \cite{contextual_walk, cs224} on this domain.

While results from neural net predictions \cite{neural_network} are very accurate, the method used itself doesn’t provide much insight on the underlying network dynamics. For example, authors of \cite{neural_network} first use an algorithm to determine score for each member that participated in the movie, then use a neural network to automatically determine weights, with which each score of the cast’s member would influence the rating prediction. However, they don’t normalize actors’ scores depending on the genre or the number of shows/movies they participated in. This leads to skewed results as actors that specialize in high-volume low-quality TV genres like shows and teen comedies or actors that play a lot of minor roles in otherwise quality films end up having better scores.

Similarly, general network analysis algorithms based on node adjacency matrix don't yield good results \cite{cs224}, mostly because quantity is not heavily correlated with quality (talk shows getting highest rankings). Authors of some other research \cite{contextual_walk} acknowledge the fact that scores depend on the genre of the movie/show, by coming up with their own algorithm called {\it ContextWalk}. It is a derivation of {\it random walk} algorithm over a five-type multipartite graph with bias depending on the rating, tags, actors and genre which they believe would improve the prediction. Authors don’t provide an actual implementation of the algorithm, but merely how {\it ContextWalk} should be implemented along with some explanation of the smart decisions they made while coming up with the algorithm. However, this algorithm is more focused on recommending the user new movies/shows by simulating a distracted user that randomly browses trough movie database site with random walks.

We use ideas from \cite{contextual_walk} to further the work done by \cite{cs224} who have the same goals as we do. Authors of \cite{cs224} use {\it HITS} \cite{hits} and {\it PageRank} \cite{pagerank} algorithms for prediction with varied success. They create two bipartite graphs, one with films connected to actors and directors that are involved in it, and the other with films connected to genres as they argue that these directly influence the final rating. Their implementation of {\it HITS} algorithm gives very good results, while {\it PageRank} doesn’t, as the highest scoring nodes are shows with lots of contributing actors.

\section{Data}

We use IMDb database\cite{imdb} which consists of several tables:
\begin{labeling}{ratings}
 \item[titles] provide basic information about titles. From here we extract information about the type of the film (e.g. movie, short movie, series, episode), its genres, and the year of release.
 
 \item[cast] contains information about the cast and other people that have contributed to the film. 
 
 \item[ratings] table gives us information about movie average rating and number of votes it received.
\end{labeling}

There are many films that aren’t eligible for our models. We exclude films that are not of type “movie” and whose year of release is earlier than 1970. Films with small number of votes aren’t representing the rating well, so we remove films with number of votes less than 100. We also remove films that don’t have a common person with any other film, leaving us with a network of 9 524 vertices.

For entire paper we use one mode projection on the films, in which two films are connected if they share at least one person. We also add contextual information to edges by weighting them more if they share genre, have greater number of shared people or actors in main roles.

\section{Prediction models}

\subsection{Node2vec}

By running {\it node2vec}\cite{node2vec} algorithm we try to predict films’ ratings based on neighborhood of the film in our network as well as by its structural role. Random walk is simulated by two parameters, {\it p} and {\it q}, first one being a probability of returning and the second one a probability of going further away from the starting node. When deciding, what probability some neighbor has, we only consider node from which we arrive. Probability of going back is p, probability of going to some neighbor of previous node is 1, and probability of going to any other neighbor of current node is q. After calculating these probabilities, we normalize them, and by generating a random number we decide for the next node. 

{\it Node2vec} returns a list of (possibly repeated) nodes we meet by random walks. Because making an embedding only from these values would result in high dimensionality, we use {\it word2vec}\cite{word2vec} algorithm from {\it gensim}\cite{gensim} library to create the actual embedding, while limiting the number of dimensions to 200. Then we use {\it Random forest regressor}\cite{randomforest} from {\it sklearn}\cite{scikit-learn} to generate the actual predictions on ratings. 

\subsection{Random Walks}

To test the hypothesis that ratings of films depend more on a neighborhood of a film than its structural role, we also implement predictor using random walks. Our hypothesis is that if two films are more correlated, random walk will get to it more frequently than to some other node. That is for neighbors obvious. Every random walk has at least one neighbor of starting node in it, and neighbors are correlated from the definition of the network, they have same person playing in both. Similar holds for the whole neighborhood. 

We simulate 100 random walks of length six from each node and after they are simulated, each node has a sparse vector containing how many times some other node has appeared in random walks from it.  

Prediction for some node is then made by averaging ratings of visited nodes weighted by how many times we have encountered it. This is both efficient and comprehensible, because we know exactly where these weights came from, which is the opposite of predicting after Node2vec. 

\section{Results}

\section{Node2vec}

We divide the data into two parts, training and testing set. After performing random walks with parameters {\it p} and {\it q} and creating the embedding with Word2vec, we fitted the Random Forest regressor on training set and predicted ratings on testing set. MAE of our model is 0.7, which means, that the difference between predicted rating and real one is in average 0.7.  

For the reference we have created two baseline models, first one being just the average predictor, which always predicts the average, and the second one being predictor that predicts average of neighbors. Scores in Table \ref{tab:results} show us that the first one has MAE of 0.8, which means that Node2vec works, but is beaten by the neighbors’ average predictor, which has MAE of 0.73.  


\subsection{Random Walks} 

Because random walks don’t depend on rating, we first simulate the random walks and then perform leave-one-out cross-validation of our model, that is, for each node we forget its rating and then predict it. This method turned out to be more successful than Node2vec and is even better than our second baseline model, neighbors’ average predictor. If the walk is of length two, then it becomes neighbors’ average predicto. From the Graph \ref{fig:walk_length} we can observe that the global minimum of MAE is achieved when performing deeper walks of length six. Using this length we were able to produce results as shown in Table \ref{tab:results}. Distribution of predicted ratings seen in Graph \ref{fig:prediction_distribution} also show us that our model closely follows one of the training data.

We also perform a robustness test of the method by removing some proportion of edges and then rerunning predictors again. As you can see on the Graph \ref{fig:removed_edges} , Random Walks is more robust to removal of edges, and that accuracy of neighbors’ average predictor falls faster.




\begin{center}
 \begin{table}[h]
  \centering
  \begin{tabularx}{\linewidth}{l | c c c c}
    & True ratings & Random Walks & Avg. Predictor & Neighbors’ Avg. \\
    \hline
    mean     & 6.59 & 6.82 & 6.59 & 6.78\\
    variance & 1.15 & 0.40 & 0    & 0.76\\
    MAE      & /    & 0.69 & 0.81 & 0.73\\
    MSE      & /    & 0.91 & 1.15 & 1.03\\
  \end{tabularx}
  \caption{results of different classifiers.}
  \label{tab:results}
 \end{table}

\end{center}

\FloatBarrier

\begin{figure}
  \centerline{\includegraphics[width=80ex]{"../figures/removed_edges"}}
  \caption{change of MAE w.r.t. proportion of removed edges.}
  \label{fig:removed_edges}
\end{figure}

\begin{figure}
  \centerline{\includegraphics[width=80ex]{"../figures/walk_length"}}
  \caption{change of MAE w.r.t. walk length.}
  \label{fig:walk_length}
\end{figure}

\begin{figure}
  \centerline{\includegraphics[width=80ex]{"../figures/prediction_distribution"}}
  \caption{histogram of rating predictions by different predictors..}
  \label{fig:prediction_distribution}
\end{figure}

\section{Conclusion}

After all the work we have done we are sure of one thing, which is, that connection between ratings and the cast exists, and that films can be predicted from the people that contributed to it. That implies from the fact that both Node2vec and Random Walks algorithms produced better predictions than Average Predictor. And more, we show, that this connection is deeply embedded in the network, that rating of some film can be predicted better if not looking only at its cast, but rather considering films as nodes in network and performing analysis on it.

\bibliographystyle{plain} 
\bibliography{bibliografija}

\end{document}


