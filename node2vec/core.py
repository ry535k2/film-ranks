from random import choice

from node2vec.helpers import feedback


def get_next_uniformly(current):
    return choice(current.edges)


def simulate_walks(vertices, *, no_walks, walk_len):
    print('node2vec')

    for v in vertices:
        feedback(v.id / len(vertices))

        for i in range(no_walks):
            current = v

            for j in range(walk_len - 1):
                previous, current = current, get_next_uniformly(current)
                v.walks[current.id] += 1

        # normalizing
        if v.id in v.walks: del v.walks[v.id]
        total = sum(v.walks.values())
        for w in v.walks:
            v.walks[w] /= total



