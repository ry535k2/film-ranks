"""
File contains functions that are not connected to `node2vec` itself.
"""

from random import shuffle
from time import time
import numpy as np
import matplotlib.pyplot as plt
import networkx as nx


from node2vec.classes2 import *
from reduce_network import read
from network_analysis import get_connected_components

embedding_path = 'node2vec/embedding'
last_report = 0


def feedback(prop):
    global last_report
    t = time()
    if t - last_report > 2:
        last_report = t
        print(round(prop * 100, 1), '%')


def get_network(**kwargs):
    films, _, _ = read(**kwargs)
    films = get_connected_components(films)[0]

    for i, film in enumerate(films):
        film.id = i
    vertices_all = [V(film) for film in films]

    # Adding edges.
    for v1 in vertices_all:
        for edge in v1.film.edges:
            person = edge.person
            for edge2 in person.edges:
                film = edge2.film
                if v1.id != film.id:
                    v2 = vertices_all[film.id]
                    v1.edges.append(v2)
                    for i in range(len(v1.film.genres.intersection(v2.film.genres)) * 2):
                        v1.edges.append(v2)

    vertices = [v for v in vertices_all if len(v.edges) > 0]
    for i, v in enumerate(vertices):
        v.id = i

    del vertices_all

    print('One mode projection of the biggest connected component:')
    print('no. films:', len(vertices))
    return vertices


def predict(vertices):
    print('\nPredicting.')

    y = []

    for v in vertices:
        rating = 0
        for id in v.walks:
            rating += v.walks[id] * vertices[id].film.avg_rating
        y.append(rating)

    return y


def MAE(y1, y2):
    assert len(y1) == len(y2) and len(y1) > 0
    error = 0
    for i in range(len(y1)):
        error += abs(y1[i] - y2[i])
    return round(error / len(y1), 3)


def MSE(y1, y2):
    assert len(y1) == len(y2) and len(y1) > 0
    error = 0
    for i in range(len(y1)):
        error += (y1[i] - y2[i])**2
    return round(error / len(y1), 3)


def mean_variance(y):
    mean = sum(y) / len(y)
    variance = np.var(y, dtype=np.float64)

    return (mean, variance)


def vertices_to_networkx(vertices):
    G = nx.Graph()

    for v in vertices:
        G.add_node(v.id)
        number = {}
        for e in v.edges:
            if e.id > v.id:
                if e.id not in number:
                    number[e.id] = 1
                else:
                    number[e.id] = number[e.id] + 1

        for neigh_id, weight in number.items():
            G.add_edge(v.id, neigh_id, weight=weight)

    return G


def plot_distribution(walk_predicted, neighbour_predicted, train_data):
    plt.hist([train_data, walk_predicted, neighbour_predicted])
    plt.title("Distribution of ratings and predictions")
    plt.legend(["ratings", "predicted with Random Walks", "average of neighbors"])
    plt.xlabel("Rating")
    plt.ylabel("Number of movies")
    plt.savefig('figures/prediction_distribution.png')
    plt.show()


def print_results(true_data, predicted_data, prediction_type):
    mean_var = mean_variance(predicted_data)
    print("{}:\nmean {}\nvariance {}".format(prediction_type, round(mean_var[0], 3), mean_var[1]))
    print("MAE {}".format(MAE(true_data, predicted_data)))
    print("MSE {}\n".format(MSE(true_data, predicted_data)))


def evaluate(vertices, y_predicted):
    y_true = [v.film.avg_rating for v in vertices]
    mean_var = mean_variance(y_true)
    print("input data:\nmean {}\nvariance {}\n".format(round(mean_var[0], 3), mean_var[1]))

    print_results(y_true, y_predicted, "random walks")

    shuffled = list(y_true)
    shuffle(shuffled)
    print_results(y_true, shuffled, "random classifier")

    avg = sum(y_true) / len(y_true)
    y_avg = [avg for _ in range(len(y_true))]
    print_results(y_true, y_avg, "average classifier")

    y_neigh_avg = []
    for v in vertices:
        neighbours = [n.film.avg_rating for n in v.edges]
        y_neigh_avg.append(sum(neighbours) / len(neighbours))
    print_results(y_true, y_neigh_avg, "neighbours\' average classifier")

    graph_networkx = vertices_to_networkx(vertices)
    hub, auth = nx.hits(graph_networkx, max_iter=1000)
    y_neigh_hits = []
    for v in vertices:
        neighbours = [(n.film.avg_rating, n.id) for n in v.edges]
        weighted_ratings = [auth.get(n[1]) * n[0] for n in neighbours]
        weights = [auth.get(n[1]) for n in neighbours]
        y_neigh_hits.append(sum(weighted_ratings) / sum(weights))

    print_results(y_true, y_neigh_hits, "neighbours\' HITS weighted average classifier")

    plot_distribution(y_predicted, y_neigh_avg, y_true)
    # TODO: Print variance and mean of all classifiers.
