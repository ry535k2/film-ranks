from node2vec.core import *
from node2vec.helpers import *

if __name__ == '__main__':
    vertices = get_network(films_max=10000, no_votes_min=100, known_for_min=2)
    simulate_walks(vertices, no_walks=100, walk_len=6)

    y_predicted = predict(vertices)
    evaluate(vertices, y_predicted)
