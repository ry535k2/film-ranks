from collections import defaultdict


class V:
    """
    Vertex class.
    """

    def __init__(self, film):
        self.film = film
        self.id = film.id
        self.edges = []
        self.walks = defaultdict(int)
        self.d = 0

    def __eq__(self, other):
        if isinstance(other, V):
            return self.id == other.id
        return False

    def __ne__(self, other):
        return not self.__eq__(other)

    def __hash__(self):
        return self.id

    def __str__(self):
        return str(self.id) + ' -> ' + ', '.join([str(e.id) for e in self.edges])
